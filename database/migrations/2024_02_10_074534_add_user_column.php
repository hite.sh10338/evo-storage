<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('profile')->after('password')->nullable();
            $table->string('designation')->after('profile')->nullable();
            $table->string('role')->after('designation')->nullable();
            $table->string('birth_date')->after('role')->nullable();
            $table->string('mobile_number')->after('birth_date')->nullable();
            $table->string('instagram')->nullable()->after('mobile_number')->nullable();
            $table->string('twitter')->after('instagram')->nullable();
            $table->string('facebook')->after('twitter')->nullable();
            $table->string('linkedin')->after('facebook')->nullable();
            $table->string('address')->after('linkedin')->nullable();
            $table->string('about')->after('address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
};
