<?php

namespace Database\Seeders;

use App\Models\Method;
use App\Models\Module;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    public function run(): void
    {
        $module =  Module::whereName('user menu')->first();
        if(empty($module))
        {
            $module =  Module::create([
                'name' => 'user menu',
                'parent' => '0',
                'status' => 1
            ]);

            $module2 = Module::create([
                'name' => 'user',
                'parent' => $module->id,
                'status' => 1
            ]);

            $methodData = ['user-index' ,'user-add' ,'user-edit' ,'user-delete', 'user-view'];
            foreach ($methodData as $methodName) {
                Method::create([
                    'module_id' => $module2->id,
                    'route_name' => $methodName,
                    'published' => 1,
                    'status' => 1
                ]);
            }
        }
    }
}
