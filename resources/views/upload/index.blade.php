@extends('home')

@section('content')
    <div class="pagetitle">
        <div class="d-flex justify-content-between">
            <h1>File Upload</h1>

            <a href="{{ route('upload.add') }}" class="btn btn-dark">File Upload</a>
        </div>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active">File Upload</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <div class="row">
        @if($image->isEmpty())
            <div class="text-center col-lg-12">
                <b># No image data available.</b>
            </div>
        @else
            @foreach ($image as $img)
                <div class="col-sm-4">
                    <div class="card">
                        <img src="{{ asset('storage/Image_Upload/'.$img->image) }}"
                            class="card-img-top" alt="Uploaded Image">
                        <div class="card-body">
                            <h5 class="card-title">Special title treatment</h5>
                            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                            <div class="d-flex justify-content-end">
                                <span><a href="" class="btn btn-dark" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"><i class="bi bi-pen"></i></a></span>&nbsp;
                                <span><a href="" class="btn btn-dark" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View"><i class="bi bi-eye"></i></a></span>&nbsp;
                                <span><a href="{{ route('upload.remove',$img->id) }}" class="btn btn-dark" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Remove"><i class="bi bi-trash"></i></a></span>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
    <div>
      {{ $image->links('pagination::bootstrap-5') }}
    </div>
@endsection
