@extends('home')

@section('content')
    <div class="pagetitle">
            <h1>File Upload Add</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active">File Upload Add</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <div class="card">
        <div class="card-body p-3">
            <form class="row g-3 needs-validation" novalidate action="{{ route('upload.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="col-md-12">
                  <label for="validationCustom01" class="form-label">Image</label>
                  <input type="file" class="form-control" id="validationCustom01" name="image" required>
                  <div class="valid-feedback">
                    Image Is uploading....
                  </div>
                </div>
                <div class="col-12">
                  <button class="btn btn-primary" type="submit">Submit form</button>
                </div>
              </form>
        </div>
    </div>
@endsection
