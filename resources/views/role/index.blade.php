@extends('home')

@section('content')
    <div class="pagetitle">
        <div class="d-flex justify-content-between">
            <h1>Role</h1>

            <a href="{{ route('roles.add') }}" class="btn btn-dark">Add Role</a>
        </div>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active">Role</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">

        <div class="card">
            <div class="card-body p-3">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Role Name</th>
                            <th scope="col">Role Slug</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($roles))
                        @foreach($roles as $key=>$role)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $role->name }}</td>
                            <td>{{ $role->slug }}</td>
                            <td>{{ ($role->status == 0)?"Deactive" : "Active" }}</td>
                            <td>
                                <a class="btn btn-dark" href="{{ route('roles.edit', $role->id) }}"><i class="fa fa-pencil"></i></a>
                                <a class="btn btn-dark" href="{{ route('roles.destroy', $role->id) }}" onclick="event.preventDefault(); document.getElementById('delete-form-{{ $role->id }}').submit();"><i class="fa fa-trash" style="margin-left: 5px;"></i></a>
                                <form id="delete-form-{{ $role->id }}" action="{{ route('roles.destroy', $role->id) }}" method="POST" style="display: none;">
                                    @csrf
                                    @method('DELETE')
                                </form>
                                <a href="{{ route('roles.view', $role->id) }}" type="button" class="btn btn-dark">
                                    <i class="fa fa-eye"></i>
                                  </a>
                            </td>

                        </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection
