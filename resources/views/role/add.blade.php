@extends('home')

@section('content')
<style>
    :root {
        --main-bg: #1d2a3b;
        --main-text: #1d2a3b;
        --view: #1d2a3b;
    }

    .btn-view {
        background: var(--view) !important;
    }

    .error {
        color: #dc3545;
    }

    .error-edit {
        color: red;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <div class="content-header">
        <div class="container-fluid d-flex justify-content-between">
            <div class="row mb-2 col-lg-6">
                <div class="col-sm-6 d-flex flex-column">
                    <h4 class="m-0">Add Roles </h4>
                </div>
            </div>
        </div>
        <ol class="breadcrumb ml-2">
            <li class="breadcrumb-item"><a class="text-dark" href="{{ route('home') }}"><b>Home</b></a></li>
            <li class="breadcrumb-item active"><b>Roles </b> </li>
            <li class="breadcrumb-item active"><b>Add Roles </b> </li>
        </ol>
    </div>
    <!-- /.content-header -->

    <div class="container">
        <div class="card">
            <div class="card-body p-3">
                <form action="{{ route('roles.addRole') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="name">Role Name</label>
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror " id="name" placeholder="Enter Role Name">
                        @error('name')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="slug">Slug</label>
                        <input type="text" name="slug" class="form-control @error('slug') is-invalid @enderror" id="slug" placeholder="Enter Slug">
                        @error('slug')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="name">Status</label>
                        <br>
                        <span for="Active">Active</span>
                        <input type="radio" name="status" value="1" checked id="status_active">
                        <span for="Deactive">Deactive</span>
                        <input type="radio" name="status" value="0" id="status_deactive">

                        @error('status')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <div class="accordion custom-accordion" id="accordionExample">
                            @foreach($modules as $module)
                            <div class="accordion-item">
                                <div class="accordion-header" id="headingOne">
                                    <button class="accordion-button btn btn-outline-dark" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne_{{$module->id}}" aria-expanded="true" aria-controls="collapseOne">
                                        {{ ucfirst($module->name) }}
                                    </button>
                                </div>
                                <div id="collapseOne_{{$module->id}}" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        @foreach($module->submodules->chunk(4) as $modulechunk)
                                        <div class="row clearfix" style="margin-bottom:6px;">
                                            @foreach($modulechunk as $submodule)
                                            <div class="col-md-12 subdiv">
                                                <ul>
                                                    <li class="list-group-item " style="padding: .10rem 0.75rem;">
                                                        {{-- <input type="checkbox" class="custom-CheckBox" value="" id="box-{{$submodule->id}}">
                                                        <label for="box-{{$submodule->id}}">Select All {{ $submodule->name }}</label> --}}
                                                        @if(!empty($submodule->methods) && count($submodule->methods) > 0)
                                                            @foreach($submodule->methods as $method)
                                                                <div class="form-check form-switch">
                                                                    <input type="checkbox" class="form-check-input" name="checked[{{ $method->route_name }}]" value="" id="box-{{$submodule->id.$method->id}}">
                                                                    <label for="box-{{$submodule->id.$method->id}}">{{ $method->route_name }}</label>
                                                                </div>
                                                            @endforeach
                                                        @endif
                                                    </li>
                                                </ul>
                                            </div>
                                            @endforeach
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="form-group d-flex justify-content-center p-3">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endsection

    @push('scripts')
    <script>
        $(document).ready(function() {
            $("#roleAdd").validate({
                rules: {
                    name: "required",
                },
                errorPlacement: function(error, element) {
                    var name = $(element).attr("name");
                    error.appendTo($("#" + name + "_validate"));
                }
            });
            $('.editRole').each(function() {
                $(this).validate({
                    rules: {
                        name: "required",
                    },
                    errorPlacement: function(error, element) {
                        var name = $(element).attr("id");
                        error.appendTo($("#" + name + "_validate"));
                    }
                });
            });
        });

        $('li :checkbox').on('click', function() {
            var $chk = $(this),
                $li = $chk.closest('li'),
                $ul, $parent;
            if ($li.has('ul')) {
                $li.find(':checkbox').not(this).prop('checked', this.checked)
            }
            do {
                $ul = $li.parent();
                $parent = $ul.siblings(':checkbox');
                if ($chk.is(':checked')) {
                    $parent.prop('checked', $ul.has(':checkbox:not(:checked)').length == 0)
                } else {
                    $parent.prop('checked', false)
                }
                $chk = $parent;
                $li = $chk.closest('li');
            } while ($ul.is(':not(.someclass)'));
        });

        // Add event listener to the add form
        document.getElementById('roleAdd').addEventListener('submit', (event) => {
            event.preventDefault();

            // Check if the form fields are filled
            var exampleInputName = document.getElementById('exampleInputName');
            if (exampleInputName.value !== '') {
                $("#LoaderId").show();
                event.target.submit();
            }
        });

        // Add event listener to the update form
        document.getElementById('editRole').addEventListener('submit', (event) => {
            event.preventDefault();

            var question_type = document.getElementById('role_name');
            if (question_type.value !== '') {
                $("#LoaderId").show();
                event.target.submit();
            }
        });
    </script>
    @endpush()
