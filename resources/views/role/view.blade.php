@extends('home')

@section('content')
    <div class="pagetitle">
        <div class="d-flex justify-content-between">
            <h1>View Role</h1>
        </div>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active">View Role</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">

        <div class="card basic">
            <div class="card-body">
              <div class="d-flex justify-content-between">
                <h3 class="card-title">{{ $role->name }}</h3>
                <div class="d-flex justify-content-between col-lg-1">
                    <h3 class="card-title">{{ $role->slug }}</h3>
                    @if ($role->status == 1)
                        <h5 class="card-title"><i class="fa fa-circle text-success"></i></h5>
                        @else
                        <h5 class="card-title"><i class="fa fa-circle text-danger"></i></h5>
                    @endif
                </div>
              </div>

              <div>
                <h3 class="card-title"><i class="fa fa-hashtag"></i> Permissions</h3>
                <h4>
                    {{ $role->permissions }}
                </h4>
              </div>

            </div>
          </div>
    </section>
@endsection
