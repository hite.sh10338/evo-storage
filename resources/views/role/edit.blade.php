@extends('home')

@section('content')
    <div class="pagetitle">
        <div class="d-flex justify-content-between">
            <h1>Role</h1>
        </div>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active">Role</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">

        @php
            $permisionArr = json_decode($singleRoleData->permissions, true);
        @endphp
        <div class="card">
            <div class="card-body p-3">
                <form action="{{ route('roles.update', $singleRoleData->id) }}" class="editRole" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="role_id" id="role_id" value="{{$singleRoleData->id}}">
                    @method('PUT')
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @elseif (session('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ session('error') }}
                    </div>
                    @endif
                    <div class="form-group">
                        <label for="name">Role Name</label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="Enter Role Name" value="{{ $singleRoleData->name }}">
                        @error('name')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="name">Slug</label>
                        <input type="text" name="slug" class="form-control" id="slug" placeholder="Enter Slug" value="{{ $singleRoleData->slug }}">
                        @error('slug')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="name">Status</label>
                        <br>
                        <span for="Active">Active</span>
                        <input type="radio" name="status" value="1" {{ $singleRoleData->status == 1 ? 'checked' : ''}} id="status_active">
                        <span for="Deactive">Deactive</span>
                        <input type="radio" name="status" value="0" {{ $singleRoleData->status == 0 ? 'checked' : ''}} id="status_deactive">

                        @error('status')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group" id="editrole">
                        <input type="hidden" id="roleData_{{$singleRoleData->id}}" value="{{ $singleRoleData->permissions }}" />
                        <div class="card-body">
                            <div class="accordion custom-accordion" id="accordionExamplenew">

                                @foreach($modules as $module)
                                <div class="accordion-item">
                                    <div class="accordion-header" id="headingtwo">
                                        <button class="accordion-button btn btn-outline-dark" style="width: 200px; margin: 10px 10px" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne_{{$module->id}}" aria-expanded="true" aria-controls="collapseOne">
                                            {{ ucfirst($module->name) }}
                                        </button>
                                    </div>
                                    <div id="collapseOne_{{$module->id}}" class="accordion-collapse collapse" aria-labelledby="headingtwo" data-bs-parent="#accordionExamplenew">
                                        <div class="accordion-body">
                                            @foreach($module->submodules->chunk(4) as $modulechunk)
                                            <div class="row clearfix" style="margin-bottom:6px;">
                                                @foreach($modulechunk as $submodule)
                                                <div class="col-md-12 subdiv">
                                                    <ul>
                                                    <li class="list-group-item bg-gray text-white" style="padding: .10rem 0.75rem;">
                                                        {{-- <input type="checkbox" id="box-edit-{{$module->id}}-{{$submodule->id}}" attr-id="{{$module->id}}_{{$submodule->id}}" class="custom-CheckBox parentCheckBoxChecked" value="">
                                                        <label for="box-edit-{{$module->id}}-{{$submodule->id}}">Select All {{ $submodule->name }}</label> --}}

                                                            @if(!empty($submodule->methods) && count($submodule->methods) > 0)
                                                            {{-- <ul class="list-group text text-secondary" style="margin: unset;"> --}}
                                                                @foreach($submodule->methods as $method)
                                                                <div class="form-check form-switch">
                                                                    <input type="checkbox" id="box-edit-{{$submodule->id.$method->id}}" attr-route-name="{{ $method->route_name }}" class="form-check-input checkBoxChecked_{{$module->id}}_{{$submodule->id}}" name="checked[{{ $method->route_name }}]" value="" @if(array_key_exists($method->route_name,$permisionArr)) checked @endif>
                                                                    {{--  &nbsp;  --}}
                                                                    <label for="{{$submodule->id.$method->id}}">{{ $method->route_name }}</label>
                                                                </div>
                                                                @endforeach
                                                            {{-- </ul> --}}
                                                            @endif
                                                        </li>
                                                    </ul>
                                                </div>
                                                @endforeach
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="form-group d-flex justify-content-center">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
