@extends('home')

@section('content')
    <div class="pagetitle">
        <h1>Add TimeTrack</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active">TimeTrack</li>
                <li class="breadcrumb-item active">Add TimeTrack</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section profile">
        <div class="card">
            <div class="card-body p-3">
                <form class="row g-3" action="{{ route('time-track.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-6">
                        <label for="inputEmail4" class="form-label">Card Name</label>
                        <input type="text" name="card_name" class="form-control" id="inputEmail4">
                    </div>
                    <div class="col-md-6">
                        <label for="inputPassword4" class="form-label">Time</label>
                        <input type="time" name="card_time" class="form-control" id="inputPassword4">
                    </div>
                    <div class="col-12">
                        <label for="inputAddress" class="form-label">Card Discription</label>
                        <textarea name="card_dis" class="form-control" cols="30" rows="5"></textarea>
                    </div>
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary">submit</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
