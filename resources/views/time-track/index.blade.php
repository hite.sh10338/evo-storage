@extends('home')

@section('content')

    <div class="pagetitle">
        <div class="d-flex justify-content-between">
            <h1>TimeTrack</h1>

            <a href="{{ route('time-track.add') }}" class="btn btn-dark">Add TimeTrack</a>
        </div>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active">TimeTrack</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section profile">
        <div class="row row-cols-1 row-cols-md-2 g-4">
            @if($time_track->isEmpty())
                <div class="text-center col-lg-12">
                    <b># No time track data available.</b>
                </div>
            @else
                @foreach ($time_track as $time)
                    <div class="col">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <h5 class="card-title">{{ ucfirst($time->card_name) }}</h5>
                                    <h5 class="card-title">Spent Time : {{ $time->card_time }}</h5>
                                </div>
                                <p class="card-text">{{ \Illuminate\Support\Str::limit(html_entity_decode(strip_tags($time->card_dis)), 120, $end=' read more') }}</p>
                                <div class="d-flex justify-content-end">
                                    <span><a href="" class="btn btn-dark" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"><i class="bi bi-pen"></i></a></span>&nbsp;
                                    <span><a href="" class="btn btn-dark" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View"><i class="bi bi-eye"></i></a></span>&nbsp;
                                    <span><a href="{{ route('time-track.remove',$time->id) }}" class="btn btn-dark" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Remove"><i class="bi bi-trash"></i></a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif

          </div>
          <div>
            {{ $time_track->links('pagination::bootstrap-5') }}
          </div>
    </section>
@endsection
