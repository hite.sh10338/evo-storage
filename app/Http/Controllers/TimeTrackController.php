<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\TimeTrack;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TimeTrackController extends Controller
{
    public function index() {
        if(Auth::user()->role == 'Admin') {
            $time_track = TimeTrack::paginate(4);
        } else {
            $time_track = TimeTrack::where('org_id', Auth::user()->id)->paginate(4);
        }

        return view('time-track.index', compact('time_track'));
    }

    public function add() {
        return view('time-track.add');
    }

    public function store(Request $request) {
        $data = $request->all();
        $data['org_id'] = Auth::user()->id;
        TimeTrack::create($data);

        return redirect()->route('time-track.index')->with('success', 'card create succesfully');
    }

    public function remove($id) {
        $time_track = TimeTrack::find($id);
        $time_track->delete();

        return redirect()->back()->with('success', 'card remove succesfully');
    }
}
