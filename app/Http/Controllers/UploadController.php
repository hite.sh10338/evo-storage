<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    public function index()
    {
        if(Auth::user()->role == 'Admin') {
            $image = Upload::paginate(9);
        } else {
            $image = Upload::where('org_id', Auth::user()->id)->paginate(9);
        }
        return view('upload.index', compact('image'));
    }

    public function add()
    {
        return view('upload.add');
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,gif',
            ]);

            $file = $request->file('image');
            $fileName   = 'Image' . time() . '.' . $file->getClientOriginalExtension();
            Storage::disk('public')->put('Image_Upload/' . $fileName, File::get($file));
            $data['image'] = $fileName;
            $data['org_id'] = Auth::user()->id;

            Upload::create($data);

            return redirect()->route('upload.index')->with('success', 'Your data is stored..');
        } catch(\Exception $e){
            return redirect()->route('upload.index')->with('error',$e->getMessage());
        }
    }

    public function remove($id) {
        $image = Upload::find($id);
        $image->delete();

        return redirect()->back()->with('success', 'Your data is deleted..');
    }
}
