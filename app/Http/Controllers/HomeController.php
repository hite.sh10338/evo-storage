<?php

namespace App\Http\Controllers;

use App\Models\TimeTrack;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $time_track = TimeTrack::count();
        return view('dashbord', compact('time_track'));
    }
}
