<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function profile()
    {
        $roles = Role::get();
        return view('user.profile', compact('roles'));
    }

    public function update(Request $request, $id)
    {
        try {
            $data = [
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'mobile_number' => $request->input('mobile_number'),
                'role' => $request->input('role'),
                'address' => $request->input('address'),
                'about' => $request->input('about'),
                'designation' => $request->input('designation'),
                'instagram' => $request->input('instagram'),
                'facebook' => $request->input('facebook'),
                'twitter' => $request->input('twitter'),
                'linkedin' => $request->input('linkedin'),
            ];

            if ($request->hasFile('profile')) {
                $file = $request->file('profile');
                $fileName   = time() . '.' . $file->getClientOriginalExtension();
                Storage::disk('public')->put('user/' . $fileName, File::get($file));
                $data['profile'] = $fileName;
            }

            User::where('id', $id)->update($data);

            return redirect()->route('user.profile')->with('success', 'Profile will be updated successfully');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e);
        };
    }

}
