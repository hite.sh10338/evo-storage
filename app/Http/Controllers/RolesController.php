<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Repository\RoleRepository;
use Illuminate\Http\Request;

class RolesController extends Controller
{
    private $role;

    public function __construct(RoleRepository $role)
    {
        $this->role = $role;
    }


    public function viewRoles()
    {
        $roles = $this->role->getRoles();
        $state = false;
        return view('role.index', compact('roles','state'));
    }

    public function addRoles()
    {
        $modules = $this->role->getModuleAndMethods();
        // dd($modules);
        return view('role.add',compact('modules'));
    }

    public function addRole(Request $request)
    {
        $request->validate([
            "name" => "required|regex:/^[a-zA-Z]+$/u|max:255",
            "slug" => "required|max:255",
            "status" => "required"
        ]);

        $permission =$request->checked;
        array_walk($permission, function(&$value) {
            $value = $value === null ? true : $value;
        });

        // Prepare role data
        $data = [
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'permissions' => json_encode($permission),
            'status' => $request->input('status'),
        ];

        // Add role using the repository
        $this->role->addRole($data);

        return redirect()->route('roles.index')->with('success', 'New Role will be added....');
    }

    public function editRole($id)
    {
        try{
            $singleRoleData = $this->role->find($id);
            $modules = $this->role->getModuleAndMethods();

            // Return view with the role data for editing
            return view('role.edit', compact('singleRoleData','modules'));
        }catch(\Exception $e){
            return  $e->getMessage();
        }
        // return view('admin.roles.edit', compact('role'));
    }

    public function updateRole(Request $request, $id)
    {

        // Validate the request data
        $request->validate([
            "name" => "required|regex:/^[a-zA-Z]+$/u|max:255",
            "slug" => "required|max:255",
            "checked" => "required|max:255"
        ]);

        $permission =$request->checked;
        array_walk($permission, function(&$value) {
            $value = $value === null ? true : $value;
        });

        // Prepare role data
         $data = [
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'permissions' => json_encode($permission),
            'status' => $request->input('status'),
        ];

        // Update role using the repository
        $this->role->updateRole($data, $id);
        return redirect()->route('roles.index')->with('success', 'Roles updated successfully');
    }

    public function viewRole($id)
    {
        try{
            $role = $this->role->find($id);
            $singleRoleData = $this->role->find($id);
            $modules = $this->role->getModuleAndMethods();
            // Return view with the role data for editing
            return view('admin.roles.view', compact('role','modules'));
        }catch(\Exception $e){
            return  $e->getMessage();
        }
    }

    public function destroy($id)
    {
        // Delete the role using the repository
        $this->role->delete($id);

        // Redirect to the desired page after deleting
        return redirect()->route('roles.index')->with('success', 'Roles deleted successfully');
    }

    public function view($id)
    {
        // Delete the role using the repository
        $role = Role::find($id);

        // Redirect to the desired page after deleting
        return view('role.view', compact('role'));
    }

}
