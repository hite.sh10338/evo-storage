<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    use HasFactory;

    protected $table = 'modules';

    protected $primaryKey = 'id';
    protected $fillable = ['name','parent','status'];

    public function submodules()
    {
        return $this->hasMany(Module::class, 'parent')->where('status','1');
    }

    public function methods()
    {
        return $this->hasMany(Method::class);
    }

    public function getRollChecked(array $permisionArr = [])
    {
        $checkedCount = 0;
        foreach ($this->methods as $method) {
            if (array_key_exists($method->route_name, $permisionArr)) {
                $checkedCount++;
            }
        }
        return $checkedCount;
    }

    public function getModuleAndMethods()
    {
        return Module::select(['id','name','parent','status'])
                    ->with(['submodules:id,name,parent,status',
                    'submodules.methods:id,module_id,route_name,published,status'])
                    ->where('parent', '0')->where('status','1')->get();

    }
}
