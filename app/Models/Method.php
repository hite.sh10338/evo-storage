<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Method extends Model
{
    protected $table = 'methods';

    protected $fillable = ['module_id', 'route_name', 'published', 'status'];

    public function modules()
    {
        return $this->belongsTo(Module::class, 'module_id');
    }
}
