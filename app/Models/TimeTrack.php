<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TimeTrack extends Model
{
    use HasFactory;

    protected $fillable = [
        'card_name',
        'card_time',
        'card_dis',
        'org_id',
    ];
}
