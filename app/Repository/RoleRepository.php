<?php

namespace App\Repository;

use App\Models\Module;
use App\Models\Role;

class RoleRepository
{
    private $role;
    private $modules;

    public function __construct(Role $role, Module $modules)
    {
        $this->role = $role;
        $this->modules = $modules;
    }

    public function getRoles()
    {
        return Role::get();
    }

    public function getRolesById($id)
    {
        return Role::findOrFail($id);
    }

    public function addRole(array $data)
    {
        return Role::create($data);
    }

    public function find($id)
    {
        return $this->role->find($id);
    }

    public function updateRole($data, $id)
    {
        return $this->role->where('id', $id)->update($data);
    }

    public function delete($id)
    {
        $role = $this->role->find($id);
        return $role->delete();
    }

    public function getModuleAndMethods()
    {
        return $this->modules->getModuleAndMethods();
    }
}
