<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\Gate;

class UserPolicy
{
    use HandlesAuthorization;

    public static function userPolicies(){
        Gate::define('user-list', function($user){
            return $user->hasAccess(['user-list']);
        });

        Gate::define('user-add', function($user){
                return $user->hasAccess(['user-add']);
        });

        Gate::define('user-delete', function($user){
            return $user->hasAccess(['user-delete']);
        });

        Gate::define('user-edit', function($user){
            return $user->hasAccess(['user-edit']);
        });

        Gate::define('user-view', function($user){
            return $user->hasAccess(['user-view']);
       });


    }
}
