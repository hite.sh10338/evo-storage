<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\RolesController;
use App\Http\Controllers\TimeTrackController;
use App\Http\Controllers\UploadController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/user/profile', [UserController::class, 'profile'])->name('user.profile');
Route::post('/user/profile/update/{id}', [UserController::class, 'update'])->name('user.profile.update');

Route::get('/roles', [RolesController::class, 'viewRoles'])->name('roles.index');
Route::get('/add-role', [RolesController::class, 'addRoles'])->name('roles.add');
Route::post('/add-role', [RolesController::class, 'addRole'])->name('roles.addRole');
Route::get('/role/{team}/edit', [RolesController::class, 'editRole'])->name('roles.edit');
Route::put('/role/{id}', [RolesController::class, 'updateRole'])->name('roles.update');
Route::delete('/role/{team}', [RolesController::class, 'destroy'])->name('roles.destroy');
Route::get('/role/view/{id}', [RolesController::class, 'view'])->name('roles.view');

Route::get('/time-track', [TimeTrackController::class, 'index'])->name('time-track.index');
Route::get('/time-track/add', [TimeTrackController::class, 'add'])->name('time-track.add');
Route::post('/time-track/store', [TimeTrackController::class, 'store'])->name('time-track.store');
Route::get('/time-track/remove/{id}', [TimeTrackController::class, 'remove'])->name('time-track.remove');

Route::get('/upload/list', [UploadController::class, 'index'])->name('upload.index');
Route::get('/upload/add', [UploadController::class, 'add'])->name('upload.add');
Route::post('/upload/store', [UploadController::class, 'store'])->name('upload.store');
Route::get('/upload/remove/{id}', [UploadController::class, 'remove'])->name('upload.remove');
